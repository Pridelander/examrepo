<?php

namespace common\models;

use Yii;
use common\models\File;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;

/**
 * This is the model class for table "major".
 *
 * @property int $id
 * @property int $university_id
 * @property string $name
 * @property int $price
 * @property int $tax
 * @property string $description
 * @property int $active
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property OrderMajor[] $orderMajors
 * @property University $university
 * @property User $createdBy
 * @property User $updatedBy
 */
class Major extends ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'major';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['university_id', 'name', 'price', 'tax'], 'required'],
            [['id', 'university_id', 'price', 'tax', 'active', 'created_by', 'updated_by'], 'integer'],
            [['university_id'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['name', 'description'], 'string'],
            [['created_at', 'updated_at'], 'default', 'value' => date('Y/m/d')],
            [['created_by', 'updated_by'], 'default', 'value' => Yii::$app->user->getId(true)],
            [['active'], 'default', 'value' => 1],
            [['tax'], 'default', 'value' => 23],
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'unique'],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => University::className(), 'targetAttribute' => ['university_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'university_id' => Yii::t('common', 'University ID'),
            'name' => Yii::t('common', 'Name'),
            'price' => Yii::t('common', 'Price'),
            'tax' => Yii::t('common', 'Tax'),
            'description' => Yii::t('common', 'Description'),
            //'availability' => Yii::t('common', 'Availability'),
            //'thumbnail' => Yii::t('common', 'Thumbnail'),
            'active' => Yii::t('common', 'Active'),
            'created_by' => Yii::t('common', 'Created By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }
    
    public function getSubjectsProvider($id){
        $query=Subject::find()->where(['major_id' => $id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    public function getFilesProvider($id){
        $query=File::find()->where(['major_id' => $id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    // CartPositionInterface:
    public function getPrice()
    {
        return $this->price + $this->price * $this->tax / 100;
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderMajors()
    {
        return $this->hasMany(OrderMajor::className(), ['major_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversity()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\MajorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MajorQuery(get_called_class());
    }
}
