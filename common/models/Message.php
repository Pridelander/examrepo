<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int $user_id
 * @property string $sender_name
 * @property string $sender_email
 * @property string $subject
 * @property string $content
 * @property int $processed
 *
 * @property User $user
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $x=true;
		if(!Yii::$app->user->getId()) $x=false;
		return [
            [['id'], 'integer'],
            [['user_id'], 'integer'],
            [['subject','content'], 'required'],
            [['content'], 'string'],
            [['sender_name','subject'], 'string', 'max' => 255],
            [['sender_email'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Message ID'),
            'user_id' => Yii::t('common', 'User ID'),
            'sender_name' => Yii::t('common', 'Sender Name'),
            'sender_email' => Yii::t('common', 'Sender E-mail'),
            'subject' => Yii::t('common', 'Subject'),
            'content' => Yii::t('common', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MessageQuery(get_called_class());
    }
}
