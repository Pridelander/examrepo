<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_major".
 *
 * @property int $order_id
 * @property int $major_id
 * @property int $quantity
 *
 * @property Order $order
 * @property Major $major
 */
class OrderMajor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_major';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'major_id'], 'required'],
            [['order_id', 'major_id', 'quantity'], 'integer'],
            [['quantity'], 'default', 'value' => 1],
            [['order_id', 'major_id'], 'unique', 'targetAttribute' => ['order_id', 'major_id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('common', 'Order ID'),
            'major_id' => Yii::t('common', 'Major ID'),
            'quantity' => Yii::t('common', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'major_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\OrderMajorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderMajorQuery(get_called_class());
    }
}
