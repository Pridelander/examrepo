<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $name
 * @property int $subject_id
 * @property int $major_id
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Subject $subject
 * @property Major $major
 */
class File extends ActiveRecord
{
    
    public $inputfile; // will be used when uploading a file;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // REMEMBER. Things set automatically cannot be marked as required here. These rules are being applied right after the form is sent.
        return [
            [['major_id'],'required',
                'when' => function($model){
                    return !($model->subject_id);
                },'enableClientValidation' => false],
            [['subject_id'],'required',
                'when' => function($model){
                    return !($model->major_id);
                },'enableClientValidation' => false],
            [['id', 'subject_id', 'major_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => date('Y/m/d')],
            [['created_by', 'updated_by'], 'default', 'value' => Yii::$app->user->getId(true)],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            //[['id'], 'unique'], // don't bother your head about it - the DB will take care;
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'ext' => Yii::t('common', 'Extension'),
            'name' => Yii::t('common', 'Name'),
			'inputfile' => Yii::t('common', 'Input file'),
            'subject_id' => Yii::t('common', 'Subject ID'),
            'major_id' => Yii::t('common', 'Major ID'),
            'created_by' => Yii::t('common', 'Created By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    public function getCommentsProvider($id,$pagination=true){
        $query=Comment::find()->where(['file_id' => $id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
        ]);
        if($pagination){
            $provider->pagination=[
                'pageSize' => 20,
            ];
        }
        return $provider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'major_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\FileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\FileQuery(get_called_class());
    }
}
