<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;


class PayU{

    public static function createToken(){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://secure.snd.payu.com/pl/standard/user/oauth/authorize");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials&client_id=340287&client_secret=a6a71530242493e1543fe7f7c763010e');

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));
		$answer = curl_exec($ch);
        $ce=curl_error($ch);
		if($ce){
			var_dump($ce);
			die();
		}
        curl_close($ch);
        
        $answer=Json::decode($answer);
        return $answer['access_token'];

    }
    
    public static function createJsonData($input){
        if(is_array($input) || is_a($input,'ActiveRecord')) return Json::encode($input);
        else return null;
    }

    public static function sendJson($data,$url,$token='',$method='POST'){
        $curl = curl_init();

        if($method=='POST'){
            curl_setopt($curl, CURLOPT_POST, 1);
            if($data !== null) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }
        else{
            if ($data !== null){
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }
        }
        $headers=array("Content-Type: application/json");
        if($token){
            $auth='Authorization: Bearer ';
            $auth.=$token;
            array_push($headers,$auth);
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

}
