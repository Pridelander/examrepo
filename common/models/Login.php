<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "login".
 *
 * @property int $user_id
 * @property string $datetime
 *
 * @property User $user
 */
class Login extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['datetime','ip'], 'safe'],
            [['datetime'], 'default', 'value' => date('Y-m-d H:i:s')],
			[['ip'], 'default', 'value' => Yii::$app->getRequest()->getUserIP()],
			[['user_id', 'datetime'], 'unique', 'targetAttribute' => ['user_id', 'datetime']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'User ID'),
            'datetime' => Yii::t('common', 'Datetime'),
			'ip' => Yii::t('common', 'IP address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\LoginQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LoginQuery(get_called_class());
    }
}
