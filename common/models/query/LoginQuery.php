<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Login]].
 *
 * @see \common\models\Login
 */
class LoginQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Login[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Login|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
