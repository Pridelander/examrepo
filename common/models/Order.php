<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $client_id
 * @property int $status
 * @property string $payment_link
 * @property int $payment_type_id
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property PaymentType $paymentType
 * @property User $updatedBy
 * @property User $client
 * @property OrderMajor[] $orderMajors
 * @property Major[] $majors
 */
class Order extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
                'defaultValue' => -2, // PayU;
            ],
        ];
    }
    
	/**
     * {@inheritdoc}
     */
	public function rules()
    {
        return [
            [['id', 'client_id', 'status', 'payment_type_id', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => date('Y/m/d')],
            [['updated_by'], 'default', 'value' => Yii::$app->user->getId(true)],
            [['status'], 'default', 'value' => 1],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_link'], 'string', 'max' => 1023],
            [['payment_link'], 'unique'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'client_id' => Yii::t('common', 'Client ID'),
            'name' => Yii::t('common', 'Name'),
            'status' => Yii::t('common', 'Status'),
            'payment_link' => Yii::t('common', 'Payment Link'),
            'payment_type_id' => Yii::t('common', 'Payment Type ID'),
            'created_by' => Yii::t('common', 'Created By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }
    
    public function getOrderedMajors(){
        $connection = Yii::$app->getDb();
        $command=$connection->createCommand('SELECT m.id, m.name as major, quantity, price, tax FROM '.Major::tableName().' m JOIN '.OrderMajor::tableName().' om ON m.id=om.major_id WHERE om.order_id='.$this->id.' ORDER BY m.id DESC');
        $result=$command->queryAll();
        $majors=ArrayHelper::map($result,'id',
            function($model){
                return array($model["major"],$model["price"],$model["tax"],$model["quantity"]);
            }
        );
        return $majors;
    }

    public function getOMProvider($id,$pagination=true){
        $query=OrderMajor::find()->where(['order_id' => $id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
        ]);
        if($pagination){
            $provider->pagination=[
                'pageSize' => 20,
            ];
        }
        return $provider;
    }

    public function getOrderedSubjectsProvider($majors){
        $data=array();
        foreach($majors as $m){
            $tmpsubjects=$m->major->getSubjectsProvider($m->major_id);
            $data=array_merge($data,$tmpsubjects->getModels());
        }
        $provider=new ArrayDataProvider([
            'allModels' => $data,
            // 'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->getCreatedBy();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderMajors()
    {
        return $this->hasMany(OrderMajor::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajors()
    {
        return $this->hasMany(Major::className(), ['id' => 'major_id'])->viaTable('order_major', ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderQuery(get_called_class());
    }
}
