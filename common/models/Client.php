<?php

// DEPRECATED.

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $access_token
 * @property string $name
 * @property string $surname
 * @property int $discount
 *
 * @property User $id0
 */
class Client extends ActiveRecord
{
    
    public $lang=null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'access_token', 'discount'], 'integer'],
            [['username', 'password', 'name', 'surname'], 'string'],
            [['username', 'password', 'access_token'], 'default', 'value' => null],
            [['id'], 'unique'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'username' => Yii::t('common', 'Username'),
            'password' => Yii::t('common', 'Password'),
            'access_token' => Yii::t('common', 'Access Token'),
            'name' => Yii::t('common', 'Name'),
            'surname' => Yii::t('common', 'Surname'),
            'discount' => Yii::t('common', 'Discount'),
        ];
    }

    public function getOrdersProvider(){
        $query=Order::find()->where(['client_id' => $this->id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
            // 'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }
    
    public function getPermissionsProvider(){
        $query=UserPermissions::find()->where(['user_id' => $this->id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
            // 'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    public function getPermittedMajorsProvider($perms){
        $data=array();
        foreach($perms as $p) array_push($data,$p->getMajor()->one());
        $provider=new ArrayDataProvider([
            'allModels' => $data,
            // 'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    public function getPermittedSubjectsProvider($majors){
        $data=array();
        foreach($majors as $m){
            $tmpsubjects=$m->getSubjectsProvider($m->id);
            $data=array_merge($data,$tmpsubjects->getModels());
        }
        $provider=new ArrayDataProvider([
            'allModels' => $data,
            // 'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\ClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ClientQuery(get_called_class());
    }
}
