<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    
    // Here we add searchable fields from higher classes:
    public function attributes(){
        return array_merge(parent::attributes(), ['client.fname','client.lname']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['client.fname','client.lname'], 'string'],
            [['name','client.fname','client.lname', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $query->joinWith('client AS client');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->sort->attributes['client.fname'] = [
            'asc' => ['client.fname' => SORT_ASC],
            'desc' => ['client.fname' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['client.lname'] = [
            'asc' => ['client.lname' => SORT_ASC],
            'desc' => ['client.lname' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'status' => $this->status,
            'payment_type_id' => $this->payment_type_id,
            //'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'client.fname', $this->getAttribute('client.fname')])
              ->andFilterWhere(['like', 'client.lname', $this->getAttribute('client.lname')]);

        return $dataProvider;
    }
}
