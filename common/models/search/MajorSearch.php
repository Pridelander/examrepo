<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Major;

/**
 * MajorSearch represents the model behind the search form of `common\models\Major`.
 */
class MajorSearch extends Major
{
    // Here we add searchable fields from higher classes:
    public function attributes(){
        return array_merge(parent::attributes(), ['university.name','createdBy.username']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'university_id', 'price', 'tax', 'active', 'created_by'], 'integer'],
            [['name', 'description', 'university.name','createdBy.username'], 'string'],
            [['name', 'description', 'university.name','createdBy.username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Major::find();

        // add conditions that should always apply here

        $query->joinWith('university AS university');
        $query->joinWith('createdBy AS createdBy');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->sort->attributes['university.name'] = [
            'asc' => ['university.name' => SORT_ASC],
            'desc' => ['university.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['createdBy.username'] = [
            'asc' => ['createdBy.username' => SORT_ASC],
            'desc' => ['createdBy.username' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            // 'university_id' => $this->university_id,
            'price' => $this->price,
            'tax' => $this->tax,
        ]);

        $query->andFilterWhere(['like', 'major.name', $this->name])
            ->andFilterWhere(['like', 'major.description', $this->description])
            // ->andFilterWhere(['like', 'createdBy.username', $this->getAttribute('createdBy.username')])
            ->andFilterWhere(['like', 'university.name', $this->getAttribute('university.name')]);

        return $dataProvider;
    }
}
