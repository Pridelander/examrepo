<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subject;

/**
 * SubjectSearch represents the model behind the search form of `common\models\Subject`.
 */
class SubjectSearch extends Subject
{
    
    // Here we add searchable fields from higher classes:
    public function attributes(){
        return array_merge(parent::attributes(), ['major.name','createdBy.username']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description', 'major.name','createdBy.username'], 'string'],
            [['name', 'description', 'major.name','createdBy.username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subject::find();

        // add conditions that should always apply here
        $query->joinWith('major AS major');
        $query->joinWith('createdBy AS createdBy');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $dataProvider->sort->attributes['major.name'] = [
            'asc' => ['major.name' => SORT_ASC],
            'desc' => ['major.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['createdBy.username'] = [
            'asc' => ['createdBy.username' => SORT_ASC],
            'desc' => ['createdBy.username' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'major_id' => $this->major_id,
            // 'created_by' => $this->created_by,
            // 'created_at' => $this->created_at,
            // 'updated_by' => $this->updated_by,
            // 'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'subject.name', $this->name])
            ->andFilterWhere(['like', 'subject.description', $this->description])
            // ->andFilterWhere(['like', 'createdBy.username', $this->getAttribute('createdBy.username')])
            ->andFilterWhere(['like', 'major.name', $this->getAttribute('major.name')]);

        return $dataProvider;
    }
}
