<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\File;

/**
 * FileSearch represents the model behind the search form of `common\models\File`.
 */
class FileSearch extends File
{
    // Here we add searchable fields from higher classes:
    public function attributes(){
        return array_merge(parent::attributes(), ['subject.name','major.name','createdBy.username']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['subject.name', 'major.name', 'createdBy.username'],'string'],
            [['name', 'major.name', 'subject.name', 'createdBy.username', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find();

        // add conditions that should always apply here
        $query->joinWith('major AS major');
        $query->joinWith('subject AS subject');
        $query->joinWith('createdBy AS createdBy');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $dataProvider->sort->attributes['major.name'] = [
            'asc' => ['major.name' => SORT_ASC],
            'desc' => ['major.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['subject.name'] = [
            'asc' => ['subject.name' => SORT_ASC],
            'desc' => ['subject.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['createdBy.username'] = [
            'asc' => ['createdBy.username' => SORT_ASC],
            'desc' => ['createdBy.username' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'subject.name', $this->getAttribute('subject.name')])
            ->andFilterWhere(['like', 'major.name', $this->getAttribute('major.name')])
            ->andFilterWhere(['like', 'createdBy.username', $this->getAttribute('createdBy.username')]);
        
        return $dataProvider;
    }
}
