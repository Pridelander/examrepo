<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_permissions".
 *
 * @property int $user_id
 * @property int $major_id
 * @property int $expires
 *
 * @property User $user
 * @property Major $major
 */
class UserPermissions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_permissions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'major_id', 'expires'], 'required'],
            [['user_id', 'major_id', 'expires'], 'integer'],
            [['user_id', 'major_id'], 'unique', 'targetAttribute' => ['user_id', 'major_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'User ID'),
            'major_id' => Yii::t('common', 'Major ID'),
            'expires' => Yii::t('common', 'Expires'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'major_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\UserPermissionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserPermissionsQuery(get_called_class());
    }
}
