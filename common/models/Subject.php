<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "subject".
 *
 * @property int $id
 * @property string $name
 * @property int $major_id
 * @property string $description
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property File[] $files
 * @property User $createdBy
 * @property User $updatedBy
 * @property Major $major
 */
class Subject extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    // ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'major_id'], 'required'],
            [['id', 'major_id', 'created_by', 'updated_by'], 'integer'],
            [['major_id'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'default', 'value' => date('Y/m/d')],
            [['created_by', 'updated_by'], 'default', 'value' => Yii::$app->user->getId(true)],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            //[['id'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'major_id' => Yii::t('common', 'Major ID'),
            'description' => Yii::t('common', 'Description'),
            'created_by' => Yii::t('common', 'Created By'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    public function getFilesProvider($id){
        $query=File::find()->where(['subject_id' => $id]);
        $provider=new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $provider;
    }
    
    public static function getMajorList(){
        $connection = Yii::$app->getDb();
        $command=$connection->createCommand('SELECT m.id,m.name as major,u.name as uni FROM '.Major::tableName().' m JOIN '.University::tableName().' u ON m.university_id=u.id ORDER BY uni DESC');
        $result=$command->queryAll();
        $majors=ArrayHelper::map($result,'id',
            function($model){
                return $model["major"].' - '.$model["uni"];
            }
        );
        $arr=array(0);
        $keys=array_keys($majors);
        $keys=array_merge($arr,$keys);
        $arr=array('-----');
        $vals=array_values($majors);
        $vals=array_merge($arr,$vals);
        $majors=array_combine($keys,$vals);
        return $majors;
    }

    public static function getSubjectList(){
        $connection = Yii::$app->getDb();
        $command=$connection->createCommand('SELECT s.id,m.name as major,u.name as uni, s.name as subj FROM '.Major::tableName().' m JOIN '.University::tableName().' u ON m.university_id=u.id JOIN '.Subject::tableName().' s ON m.id=s.major_id ORDER BY uni DESC');
        $result=$command->queryAll();
        $majors=ArrayHelper::map($result,'id',
            function($model){
                return $model["subj"].' - '.$model["major"].' - '.$model["uni"];
            }
        );
        $arr=array(0);
        $keys=array_keys($majors);
        $keys=array_merge($arr,$keys);
        $arr=array('-----');
        $vals=array_values($majors);
        $vals=array_merge($arr,$vals);
        $result=array_combine($keys,$vals);
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'major_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\SubjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SubjectQuery(get_called_class());
    }
}
