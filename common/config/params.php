<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
	'statistics' => [
		'days_default' => 3, // number of days for displaying default statistics (today / yesterday / ...)
		'password' => false, // password to enter the statistics page.  If false (without quotes) - enter without password
		'authentication' => false, // if true, then statistics are available only to authenticated users.
		'auth_route' => 'site/login', // controller / action for the authentication page (default is 'site / login')
		'date_old' => 90 // delete data after x days
 ]
];
