<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'name' => Yii::t('common', 'Live through your studies!'),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'common' => 'common.php',
                        'backend' => 'backend.php',
                        'frontend' => 'frontend.php',
                    ],
                    // 'on missingTranslation' => ['\backend\modules\i18n\Module', 'missingTranslation'],
                ],
                /* Uncomment this code to use DbMessageSource
            '*'=> [
            'class' => 'yii\i18n\DbMessageSource',
            'sourceMessageTable'=>'{{%i18n_source_message}}',
            'messageTable'=>'{{%i18n_message}}',
            'enableCaching' => YII_ENV_DEV,
            'cachingDuration' => 3600,
            'on missingTranslation' => ['\backend\modules\i18n\Module', 'missingTranslation']
            ],
             */
            ],
        ],
    ],
];
