<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderMajor;
use common\models\UserPermissions;
use common\models\PayU;
use common\models\search\OrderSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ServerErrorHttpException;
use yii\web\NotFoundHttpException;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {            
        // if ($action->id == 'notification' || $action->id == 'test') {
        if ($action->id == 'notification') {
        // if ($action->id == 'test') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
	
	private function cloneRejectedOrder($id){
		$oldmodel=$this->findModel($id);
		$oldmodel->status=3; // payment rejected by PayU;
		$oldmodel->updated_by=-2; // PayU artificial account;
		$newmodel=new Order();
		$newmodel->client_id=$oldmodel->client_id;
		//$newmodel->name=$oldmodel->name;
		$newmodel->status=1;
		$newmodel->payment_type_id=$oldmodel->payment_type_id;
		//$newmodel->created_by=$oldmodel->created_by;
		$newmodel->created_at=$oldmodel->created_at;
		$newmodel->updated_by=-2; // PayU artificial account;
		$x=($oldmodel->save() && $newmodel->save());
		if(!$x){
			// var_dump($oldmodel->getErrors());
			// var_dump($newmodel->getErrors());
            // die();
            throw new ServerErrorHttpException(Yii::t('common', 'Could not clone the order.'));
        }
        $oms=OrderMajor::find()->where(['order_id'=>$id])->all();
        foreach($oms as $om){
            $om->order_id=$newmodel->id;
            $x=($om->save());
            if(!$x) throw new ServerErrorHttpException(Yii::t('common', 'Could not update cloned order data.'));
        }
		return true;
	}
	
    private function cloneDoubledOrder($id){
		$oldmodel=$this->findModel($id);
		$oldmodel->status=4; // order could not be processed by PayU;
		$oldmodel->updated_by=-2; // PayU artificial account;
		$newmodel=new Order();
		$newmodel->client_id=$oldmodel->client_id;
		//$newmodel->name=$oldmodel->name;
		$newmodel->status=1;
		$newmodel->payment_type_id=$oldmodel->payment_type_id;
		//$newmodel->created_by=$oldmodel->created_by;
		$newmodel->created_at=$oldmodel->created_at;
		$newmodel->updated_by=-2; // PayU artificial account;
		$x=($oldmodel->save() && $newmodel->save());
		if(!$x){
			// var_dump($oldmodel->getErrors());
			// var_dump($newmodel->getErrors());
            // die();	
            throw new ServerErrorHttpException(Yii::t('common', 'Could not clone the order.'));
		}
		$oms=OrderMajor::find()->where(['order_id'=>$id])->all();
        foreach($oms as $om){
            $om->order_id=$newmodel->id;
            $x=($om->save());
            if(!$x) throw new ServerErrorHttpException(Yii::t('common', 'Could not update cloned order data.'));
        }
		return true;
	}
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    // 'test' => ['POST'],
                    'notification' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination=[
            'pageSize' => 20,
        ];
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    */
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

	public function actionPaid($id,$additionalcheck=false){
        $model = $this->findModel($id);
        
        if($additionalcheck && $model->status==1){
            $model->status=2; // this means paid;
            if($model->save()){
                $uid=$model->client->id;
                $majors=OrderMajor::find()->select('major_id,quantity')->andWhere(['order_id'=>$model->id])->all();
                //$majors=ArrayHelper::toArray($majors);
                $majors=ArrayHelper::map($majors,'major_id','quantity');
                foreach($majors as $mid => $mq){
                    // check if a permission to some major already exists:
                    $up=UserPermissions::find()->where(['user_id'=>$uid,'major_id'=>$mid])->one();
                    $exptime=60*60*24*30*$mq;
                    if(!$up){
                        $up=new UserPermissions();
                        $up->user_id=$uid;
                        $up->major_id=$mid;
                        $up->expires=time()+$exptime;
                    }
                    else $up->expires+=$exptime;
                    if(!$up->save()){
                        throw new ServerErrorHttpException(Yii::t('common', 'Could not save the data to the database.'));
                        break;
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else throw new ServerErrorHttpException(Yii::t('common', 'Could not save the data to the database.'));
        }
    }

    public function actionPayu($id){
        $model=$this->findModel($id);
        if($model->payment_link) return $this->redirect($model->payment_link, 302);
        $majors=$model->getOrderedMajors();
        $amount=0; // this must be int!!!
        foreach($majors as $m){
            // the order: name, price, tax, quantity;
            $amount+=($m[1]+0.01*$m[1]*$m[2])*$m[3];
            $products[]=array('name'=>$m[0],'unitPrice'=>$m[1]*$m[2],'quantity'=>$m[3],'virtual'=>true);
        }
        $amount=(int)($amount*100);
        $data=array();
        $data["extOrderId"]=$model->id;
        $data["customerIp"]='127.0.0.1';
        $data["merchantPosId"]=340287;
        $data["notifyUrl"]="http://kocur.serveo.net/order/notification";
        $data["description"]='Empty String';
        $data["currencyCode"]='PLN';
        $data["totalAmount"]=$amount;
        $data["continueUrl"]="http://kocur.serveo.net:80/order/thanks";
        $data["products"]=$products;
        $data["buyer"]=array(
            'extCustomerId'=>$model->client->id,
            'email'=>$model->client->email,
            'firstName'=>$model->client->fname,
            'lastName'=>$model->client->lname,
        );
        $data=PayU::createJsonData($data);
        // var_dump($data);
        $answer=PayU::sendJson($data,"https://secure.snd.payu.com/api/v2_1/orders",PayU::createToken());
        $answer=Json::decode($answer);
        // var_dump($answer);
        $model->payment_link=$answer['redirectUri'];
        $model->save();
        return $this->redirect($answer['redirectUri'], 302);
    }

    public function actionNotification(){
        // read PayU headers:
        $headers=apache_request_headers();
        $payusignature=$headers["Openpayu-Signature"];
        // find PayU signature:
        $pos=strpos($payusignature,'signature=');
        $pos2=strpos($payusignature,';',$pos+1);
        if($pos2===false) $signature=substr($payusignature,$pos+10);
        else $signature=substr($payusignature,$pos+10,$pos2-$pos-10);
        // find hashing algorithm used by PayU:
        $pos=strpos($payusignature,'algorithm=');
        $pos2=strpos($payusignature,';',$pos+1);
        if($pos2===false) $algorithm=substr($payusignature,$pos+10);
        else $algorithm=substr($payusignature,$pos+10,$pos2-$pos-10);
        $rawData = file_get_contents("php://input");
        // verify if the notification is legit:
        $verificationstring=$rawData.'65b90a52bf1ca0884df3f294b19f20e6';
        switch($algorithm){
            case 'MD5':
                $computedsignature=md5($verificationstring);
                break;
            case 'SHA1':
                $computedsignature=sha1($verificationstring);
                break;
            case 'SHA256':
                $computedsignature=hash('sha256',$verificationstring);
                break;
            case 'SHA512':
                $computedsignature=hash('sha512',$verificationstring);
                break;
            default:
                throw new InvalidParamException(Yii::t('common','Unknown hashing algorithm: ').$algorithm);
                break;
        }
        if($computedsignature==$signature){
            $data=Json::decode($rawData);
            $oid=(int)($data['order']['extOrderId']);
            if($data['order']['status']=='COMPLETED') $this->actionPaid($oid,true);
            if($data['order']['status']=='CANCELED') $this->cloneRejectedOrder($oid);
            // echo $data['order']['status'];
            Yii::$app->response->statusCode = 200;
        }
        else throw new InvalidParamException(Yii::t('common','Notification signature does not match the computed one.'));
    }
    
    // public function actionTest(){
    //     echo PayU::createToken();
    // }

    public function actionClone($id){
        return $this->cloneDoubledOrder($id);
    }

    public function actionThanks($error=null){
        return $this->render('thanks',[
            'error' => $error,
        ]);
        // może zamienić to na chamski redirect na frontend później?
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
