<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Message;
use common\models\User;

/**
 * ContactForm is the model behind the contact form.
 */
class MessageAnswerer extends Model
{
    public $body;
	public $mid;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['body','mid'], 'required'],
			[['body'], 'string'],
			[['body'], 'safe'],
			[['mid'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'body' => Yii::t('common', 'Answer body'),
            'mid' => Yii::t('common', 'Message ID'),
        ];
    }
	
	protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }
    
	/**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $msg=$this->findModel($this->mid); // overriden function;
		return Yii::$app->mailer->compose()
            ->setTo($msg->sender_email)
            ->setFrom(["admin@localhost" => 'Examrepo Support'])
            ->setSubject('RE: '.$msg->subject)
            ->setHtmlBody($this->body)
            ->send();
    }
	
	public function saveMessage(){
		$model=new Message();
		$model->user_id=$this->uid;
		if($this->uid>0){
			$user=User::findIdentity($this->uid);
			$model->sender_email=$user->email;
		}
		else $model->sender_email=$this->email;
		if($this->name) $model->sender_name=$this->name;
		$model->subject=$this->subject;
		$model->content=$this->body;
		//var_dump($model);
		//var_dump($model->user_id);
		try{
			$model->save();
			//var_dump($model->getErrors());
		}
		catch(Exception $e){
			throw $e;
		}
		return (($model->save())?true:false);
		//return true;
	}
}
