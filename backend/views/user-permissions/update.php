<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserPermissions */

$this->title = Yii::t('common', 'Update User Permissions: ' . $model->user_id, [
    'nameAttribute' => '' . $model->user_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'User Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'major_id' => $model->major_id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="user-permissions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
