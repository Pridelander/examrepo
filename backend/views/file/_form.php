<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\models\Subject;

/* @var $this yii\web\View */
/* @var $model common\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php
        $form = ActiveForm::begin([
            'options'=>['enctype'=>'multipart/form-data']]); // important         
    ?>
    
    <!-- <?= $form->field($model, 'id')->textInput() ?> -->

    <?=
        $form->field($model, 'inputfile')->widget(FileInput::classname(), [
            //'options' => ['accept' => 'image/*'],
            'pluginOptions'=>['showUpload' => true,],
        ]);
    ?>

    <!-- <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'major_id')->dropDownList($this->params['majors']) ?>
    
    <?= $form->field($model, 'subject_id')->dropDownList($this->params['subjects']) ?>
    
    <!-- <?= $form->field($model, 'created_by')->textInput() ?> -->

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_by')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
