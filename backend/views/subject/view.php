<?php

use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Subject */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Subjects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$uploaded=(Yii::$app->request->get('uploaded'))?true:false;
?>

<div class="subject-view">
    
    <?php if($uploaded){ ?>
    <p class="btn btn-success center-block" style="font-size:1.3em;"> <?php echo Yii::t('backend', 'File uploaded successfully'); ?> </p>
    <?php } ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('backend', 'Upload'), ['/file/create', 'sid' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'name',
            ['attribute' => 'major_id',
            'value' => $model->major->name],
            'description:ntext',
            ['attribute' => 'created_by',
            'value' => ($model->createdBy)?$model->createdBy->username:''],
            'created_at',
            ['attribute' => 'updated_by',
            'value' => ($model->updatedBy)?$model->updatedBy->username:''],
            'updated_at',
        ],
    ]) ?>

    <br/>

    <?= GridView::widget([
        'dataProvider' => $files,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/file/view','id'=>$model->id]));
                },
            ],
            'created_at',
            'created_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}   {download}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    },
                    'download' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-download"></span>', $url, [
                                    'title' => Yii::t('common', 'Download'),
                        ]);
                    },
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/file/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/file/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/file/delete','id'=>$model->id]);
                    }
                    if ($action === 'download') {
                        return Url::to(['/file/download','id'=>$model->id]);
                    }
                }
            ],
        ],
    ]) ?>

</div>
