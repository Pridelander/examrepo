<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Subjects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Subject'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Subject Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/subject/view','id'=>$model->id]));
                },
            ],
            [
                'attribute' => 'major.name',
                'label' => Yii::t('common', 'Major Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->major->name, Url::to(['/major/view','id'=>$model->major->id]));
                },
            ],
            'description:ntext',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}   {update}    {delete}    {upload}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
							//'class' => 'glyphicon glyphicon-trash',
							'title' => Yii::t('common', 'Delete'),
							'data' => [
								'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
								'method' => 'post',
							],
						]);
                    },
                    'upload' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, [
                                    'title' => Yii::t('common', 'Upload'),
                        ]);
                    },
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/subject/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/subject/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/subject/delete','id'=>$model->id]);
                    }
                    if ($action === 'upload') {
                        return Url::to(['/file/create','sid'=>$model->id]);
                    }
                }
            ],
        ],
    ]); ?>
</div>
