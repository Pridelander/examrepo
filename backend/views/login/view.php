<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Login */

$this->title = Yii::t('common', 'Logins');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Logins'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a(Yii::t('common', 'Update'), ['update', 'user_id' => $model->user_id, 'datetime' => $model->datetime], ['class' => 'btn btn-primary']) ?> -->
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'user_id' => $model->user_id, 'datetime' => $model->datetime], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'user_id',
			['attribute' => 'username',
            'value' => ($model->user)?$model->user->username:''],
            'datetime',
			'ip:ntext',
        ],
    ]) ?>

</div>
