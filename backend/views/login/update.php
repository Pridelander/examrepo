<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Login */

$this->title = Yii::t('common', 'Update Login: ' . $model->user_id, [
    'nameAttribute' => '' . $model->user_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Logins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'datetime' => $model->datetime]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="login-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
