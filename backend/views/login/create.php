<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Login */

$this->title = Yii::t('common', 'Create Login');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Logins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
