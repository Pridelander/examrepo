<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username:ntext',
            'fname:ntext',
            'lname:ntext',
			['attribute' => 'level',
            'value' => function($model){
                switch($model->level){
                    case 1: return Yii::t('common', 'Admin');
                    case 0: return Yii::t('common', 'User');
                    default: return Yii::t('common', 'An error occured');
                }
            }],
            ['attribute' => 'language',
            'value' => function($model){
                foreach(Yii::$app->langs as $lcode => $lname){
                    if($lcode==$model->language) return $lname;
                }
				return '';
            }],
            'discount',
        ],
    ]) ?>

</div>
