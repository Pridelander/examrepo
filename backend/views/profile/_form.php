<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus'=>true]) ?>
	
    <?= $form->field($model, 'fname')->textInput() ?>

    <?= $form->field($model, 'lname')->textInput() ?>

    <?= $form->field($model, 'level')->checkbox(['label' => Yii::t('common','Admin')]) ?>
	
	<?= $form->field($model, 'language')->dropDownList($this->params['langs'],['options' => [$model->language => ['selected'=>true]]])->label(Yii::t('common','Language')) ?>				
	
    <?= $form->field($model, 'discount')->textInput() ?>
	
	<div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
