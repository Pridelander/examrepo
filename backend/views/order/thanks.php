<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

if($error) $this->title = Yii::t('common','Your order could not be completed.');
else $this->title = Yii::t('common','Your order has been completed.');
?>
<div class="site-index">

    <div class="jumbotron">
        
        <p class="lead">
            <?php 
                if($error) {
                    if($error==501) echo Yii::t('common','Your payment was rejected by PayU, thus we removed the order from our database. Please make it again.');
                    else echo Yii::t('common','Your order could not be completed. Please make it again.');
                }
                else echo Yii::t('common','Your order has been completed. Your permissions will be granted automatically as soon as PayU informs us about your payment.'); 
            ?>
        </p>
        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <p style="text-align:center;"><?= Html::a(Yii::t('common','My profile'),Url::to(['/profile/view', 'id' => Yii::$app->user->getId(true)]),['class' => "btn btn-lg btn-primary"]) ?></p>

            </div>
            <div class="col-lg-6">
                <p style="text-align:center;"><?= Html::a(Yii::t('common','My orders'),Url::to(['/order/index']),['class' => "btn btn-lg btn-primary"]) ?></p>
            </div>
        </div>

    </div>
</div>