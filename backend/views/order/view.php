<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = Yii::t('common', 'Order no. ').$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('common', 'Mark as paid'), ['/order/paid', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('common', 'PayU'), ['/order/payu', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
		<?= Html::a(Yii::t('common', 'PayU'), ['/order/clone', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'client_id',
            ['attribute' => 'client_id',
            'value' => $model->client->fname],
            ['attribute' => 'client_id',
            'value' => $model->client->lname],
            // 'name:ntext',
            // 'status',
            ['attribute' => 'status',
            'value' => function($model){
                switch($model->status){
                    case 1: return Yii::t('common', 'Created');
                    case 2: return Yii::t('common', 'Paid');
                    case 3: return Yii::t('common', 'Rejected by PayU');
                    case 4: return Yii::t('common', 'Error on PayU side');
					default: return Yii::t('common', 'An error occured');
                }
            }],
            'payment_link',
            // 'payment_type_id',
            // 'created_by',
            // 'created_at',
            ['attribute' => 'updated_by',
            'value' => ($model->createdBy)?$model->createdBy->username:''],
            // 'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
