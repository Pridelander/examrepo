<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;

$this->title = Yii::t('common', 'Answer the message no. ').$model->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer">
    <h1><?= Html::encode($this->title) ?></h1>
	
	<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'user_id',
            ['attribute' => 'created_by',
            'value' => ($model->user_id>0)?$model->user->username:''],
            'sender_name',
			'sender_email:email',
            'subject',
            'content:ntext',
			['attribute' => 'processed',
            'value' => ($model->processed==1)?Yii::t('common', 'Yes'):Yii::t('common', 'No')],
        ],
    ]) ?>
	
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'answer-form']); ?>

               <?= $form->field($ma, 'body')->textarea(['rows' => 6,'autofocus' => true]) ?>
			   
			   <?= $form->field($ma, 'mid')->hiddenInput(['value'=>$model->id])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('common', 'Send'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
