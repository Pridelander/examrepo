<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Message */

$this->title = Yii::t('common', 'Message no. ').$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
		<?= Html::a(Yii::t('common', 'Mark as processed'), ['processed', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
		<?= Html::a(Yii::t('common', 'Answer'), ['answer', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'user_id',
            ['attribute' => 'created_by',
            'value' => ($model->user_id>0)?$model->user->username:''],
            'sender_name',
			'sender_email:email',
            'subject',
            'content:ntext',
			['attribute' => 'processed',
            'value' => ($model->processed==1)?Yii::t('common', 'Yes'):Yii::t('common', 'No')],
        ],
    ]) ?>

</div>
