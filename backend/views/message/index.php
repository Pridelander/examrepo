<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create Message'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'rowOptions' => function ($model, $index, $widget, $grid){
			if($model->processed == 1) return ['class' => 'gray'];
			else return [];
		},
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            //'sender_name',
			[
				'attribute' => 'created_by',
				'value' => function ($model) {
                    return ($model->user_id>0)?$model->user->username:'';
                },
			],
            'sender_email:email',
            'subject',
            //'content:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                'template' => '{view}	{update}	{delete}	{answer}	{processed}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    },
					'answer' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-send"></span>', $url, [
                                    'title' => Yii::t('common', 'Answer'),
                        ]);
                    },
					'processed' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-check"></span>', $url, [
                                    'title' => Yii::t('common', 'Mark as processed'),
                        ]);
                    }
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/message/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/message/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/message/delete','id'=>$model->id]);
                    }
					if ($action === 'answer') {
                        return Url::to(['/message/answer','id'=>$model->id]);
                    }
					if ($action === 'processed') {
                        return Url::to(['/message/processed','id'=>$model->id]);
                    }
                },
            ],
        ],
    ]); ?>
</div>
