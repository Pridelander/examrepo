<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Major */

$this->title = Yii::t('backend', 'Create Major');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Majors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="major-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
