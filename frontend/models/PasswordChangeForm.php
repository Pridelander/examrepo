<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class PasswordChangeForm extends Model
{
    public $oldpassword;
    public $password;
    public $repeatpassword;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['oldpassword', 'required', 'message' => Yii::t('common','This field cannot be blank.')],
            
            ['password', 'required', 'message' => Yii::t('common','This field cannot be blank.')],
            ['password', 'string', 'min' => 6],

            ['repeatpassword', 'required', 'message' => Yii::t('common','This field cannot be blank.')],
            ['repeatpassword', 'string', 'min' => 6],
            ['repeatpassword', 'compare', 'compareAttribute' => 'password', 'enableClientValidation' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldpassword' => Yii::t('common', 'Current password'),
			'password' => Yii::t('common', 'New password'),
			'repeatpassword' => Yii::t('common', 'Repeat password'),
        ];
    }
	
	/**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function submit()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $uid=Yii::$app->user->getId(); // this is safe as long as we're sure a user is already logged in;
        $user=User::findIdentity($uid);
        if(!$user->validatePassword($this->oldpassword)) return null;
        $user->setPassword($this->password);
        return $user->save() ? $user : null;
    }
}
