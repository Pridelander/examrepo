<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * E-mail change form
 */
class EmailChangeForm extends Model
{
    public $oldpassword;
    public $email;
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['oldpassword', 'required', 'message' => Yii::t('common','This field cannot be blank.')],
            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('common','This email address has already been taken.')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldpassword' => Yii::t('common', 'Current password'),
			'email' => Yii::t('common', 'E-mail'),
        ];
    }
	
	/**
     * Changes the user's password.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function submit()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $uid=Yii::$app->user->getId(); // this is safe as long as we're sure a user is already logged in;
        $user=User::findIdentity($uid);
        if(!$user->validatePassword($this->oldpassword)) return null;
        $user->email = $this->email;
        return $user->save() ? $user : null;
    }
}
