<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\web\Cookie;
use common\models\User;

/**
 * Language select form
 */
class LangSelectForm extends Model
{
    public $lang;
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lang', 'required'],
            ['lang', 'string', 'max' => 10],
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'lang' => Yii::t('common', 'Language'),
        ];
    }
    
	/**
     * Saves the selected language.
     *
     * @return bool if everything went OK
     */
    public function submit()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $uid=Yii::$app->user->getId(); // this is safe as long as we're sure a user is already logged in;
        
        Yii::$app->response->cookies->add(new Cookie([
            'name' => 'app_lang',
            'value' => $this->lang,
            'expire' => time()+60*60*24*30,
        ]));
        if($uid>0){
            $user=User::findIdentity($uid);
            $user->language = $this->lang;
            return $user->save() ? true : false;
        } else return true;
    }
}
