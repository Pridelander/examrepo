<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Message;
use common\models\User;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $uid;
	public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['subject', 'body'], 'required'],
			[['name', 'email'], 'required',
				'when' => function($model){
                    return ($model->uid<=0);
                },'enableClientValidation' => false],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => Yii::t('common', 'Verification Code'),
			'name' => Yii::t('common', 'Name'),
			'email' => Yii::t('common', 'E-mail'),
			'body' => Yii::t('common', 'Content'),
			'subject' => Yii::t('common', 'Subject'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
	
	public function saveMessage(){
		$model=new Message();
		$model->user_id=$this->uid;
		if($this->uid>0){
			$user=User::findIdentity($this->uid);
			$model->sender_email=$user->email;
		}
		else $model->sender_email=$this->email;
		if($this->name) $model->sender_name=$this->name;
		$model->subject=$this->subject;
		$model->content=$this->body;
		//var_dump($model);
		//var_dump($model->user_id);
		try{
			$model->save();
			//var_dump($model->getErrors());
		}
		catch(Exception $e){
			throw $e;
		}
		return (($model->save())?true:false);
		//return true;
	}
}
