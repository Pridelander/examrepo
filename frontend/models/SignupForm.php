<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
	public $fname;
	public $lname;
	public $language;
    public $email;
    public $password;
    public $repeatpassword;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('common','This username has already been taken.')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('common','This email address has already been taken.')],

			['fname', 'string', 'min' => 2, 'max' => 40],
			
			['lname', 'string', 'min' => 3, 'max' => 50],			
			
			['language', 'required'],
			
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['repeatpassword', 'required'],
            ['repeatpassword', 'string', 'min' => 6],
            ['repeatpassword', 'compare', 'compareAttribute' => 'password', 'enableClientValidation' => false],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->fname = $this->fname;
		$user->lname = $this->lname;
		$user->email = $this->email;
		$user->language = $this->language;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        // return $user;
		$x=$user->save();
		if(!$x){
			var_dump($user->getErrors());
			die();
		}
        return $user;
    }
	
	/**
     * {@inheritdoc}
     */
	public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Username'),
            'fname' => Yii::t('common', 'First name'),
            'lname' => Yii::t('common', 'Last name'),
            'language' => Yii::t('common', 'Language'),
            'email' => Yii::t('common', 'E-mail'),
            'repeatpassword' => Yii::t('common', 'Repeat password'),
            'password' => Yii::t('common', 'Password'),
        ];
    }
	
}
