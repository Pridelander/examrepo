<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\File;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\LangSelectForm;
use frontend\models\EmailChangeForm;
use frontend\models\PasswordChangeForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'password-change', 'email-change'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'password-change', 'email-change'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
			'statistics' => [
				'class' => \Klisl\Statistics\AddStatistics::class,
				'actions' => ['signup','contact'],
			],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact($fid=null)
    {
        $model = new ContactForm();
		$model->uid=Yii::$app->user->getId(true);
        $filename=null;
		if($fid){
			$filemodel=File::findOne($fid);
			if($filemodel === null) throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
			$filename=$filemodel->name;
		}
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->uid=Yii::$app->user->getId(true);
			if($fid) $model->body=Yii::t('common', 'Report on file no.: ')."$fid \n\n".$model->body;
			if($model->saveMessage()){
			//if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('common', 'Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('common', 'There was an error sending your message.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
				'filename' => $filename,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
            // no exception here;
        }
        
        // clear the form's password fields:
        $model->password='';
        $model->repeatpassword='';
		$this->view->params['langs']=Yii::$app->langs;
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Allows user to select their language.
     *
     * @return mixed
     */
    public function actionLanguageSelect(){
        $model=new LangSelectForm();
        /*$langs=array('pl-PL' => 'polski', 'en-US' => 'American English');
        $this->view->params['langs']=$langs;*/
		$this->view->params['langs']=Yii::$app->langs;
        if ($model->load(Yii::$app->request->post())) {
            if($model->submit()){
                return $this->goBack();
            }
            else throw new ServerErrorHttpException(Yii::t('common', 'Could not change the application language.'));
        }

        return $this->render('langSelect', [
            'model' => $model,
        ]);
    }

    /**
     * Changes user's e-mail.
     *
     * @return mixed
     */
    public function actionEmailChange(){
        $model=new EmailChangeForm();
        if ($model->load(Yii::$app->request->post())) {
            if($user=$model->submit()){
                return $this->goHome();
            }
            // no exception here;
        }

        // clear the form's password fields:
        $model->oldpassword='';
        return $this->render('emailChange', [
            'model' => $model,
        ]);
    }

    /**
     * Changes user's password.
     *
     * @return mixed
     */
    public function actionPasswordChange(){
        $model=new PasswordChangeForm();
        if ($model->load(Yii::$app->request->post())) {
            if($user=$model->submit()){
                return $this->actionLogout();
            }
            // no exception here;
        }

        // clear the form's password fields:
        $model->oldpassword='';
        $model->password='';
        $model->repeatpassword='';
        return $this->render('passwordChange', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
