<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderMajor;
use common\models\PayU;
use common\models\search\OrderSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','delete','payu'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function cloneDoubledOrder($id){
		$oldmodel=$this->findModel($id);
		$oldmodel->status=4; // order could not be processed by PayU;
		$oldmodel->updated_by=-2; // PayU artificial account;
		$newmodel=new Order();
		$newmodel->client_id=$oldmodel->client_id;
		//$newmodel->name=$oldmodel->name;
		$newmodel->status=1;
		$newmodel->payment_type_id=$oldmodel->payment_type_id;
		//$newmodel->created_by=$oldmodel->created_by;
		$newmodel->created_at=$oldmodel->created_at;
		$newmodel->updated_by=-2; // PayU artificial account;
		$x=($oldmodel->save() && $newmodel->save());
		if(!$x){
			// var_dump($oldmodel->getErrors());
			// var_dump($newmodel->getErrors());
            // die();	
            throw new ServerErrorHttpException(Yii::t('common', 'Could not clone the order.'));
		}
		$oms=OrderMajor::find()->where(['order_id'=>$id])->all();
        foreach($oms as $om){
            $om->order_id=$newmodel->id;
            $x=($om->save());
            if(!$x) throw new ServerErrorHttpException(Yii::t('common', 'Could not update cloned order data.'));
        }
		return $newmodel->id;
	}
    
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['client_id'=>Yii::$app->user->getId(true)]);
        $dataProvider->query->andWhere(['order.status'=>[1,2]]);
		$dataProvider->pagination=[
            'pageSize' => 20,
        ];
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $object=$this->findModel($id);
        $oms=$object->getOMProvider($id);
        $subjects=$object->getOrderedSubjectsProvider($oms->getModels());
        
        return $this->render('view', [
            'model' => $object,
            'oms' => $oms,
            'subjects' => $subjects,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
	public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
	*/
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
	/*
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
	*/
    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
		if($model->client && $model->client->id==Yii::$app->user->getId(true) && $model->status==1) { // status 1 means "created";
			$model->delete();
        	return $this->redirect(['index']);
		} else throw new ForbiddenHttpException(Yii::t('common', 'You cannot remove this order.'));
    }

    public function actionPayu($id){
        $model=$this->findModel($id);
        if($model->status!=1) throw new ForbiddenHttpException(Yii::t('common', 'This order has already been paid.'));
        if($model->payment_link) return $this->redirect($model->payment_link, 302);
        $majors=$model->getOrderedMajors();
        $amount=0; // this must be int!!!
        foreach($majors as $m){
            // the order: name, price, tax, quantity;
            $amount+=($m[1]+0.01*$m[1]*$m[2])*$m[3];
            $products[]=array('name'=>$m[0],'unitPrice'=>$m[1]*$m[2],'quantity'=>$m[3],'virtual'=>true);
        }
        $amount=(int)($amount*100);
        $data=array();
        $data["extOrderId"]=$model->id;
        $data["customerIp"]='127.0.0.1';
        $data["merchantPosId"]=340287;
        $data["notifyUrl"]="http://kocur.serveo.net/order/notification";
        $data["description"]='Empty String';
        $data["currencyCode"]='PLN';
        $data["totalAmount"]=$amount;
        $data["continueUrl"]="http://kocur.serveo.net:80/order/thanks";
        $data["products"]=$products;
        $data["buyer"]=array(
            'extCustomerId'=>$model->client->id,
            'email'=>$model->client->email,
            'firstName'=>$model->client->fname,
            'lastName'=>$model->client->lname,
        );
        $data=PayU::createJsonData($data);
        $answer=PayU::sendJson($data,"https://secure.snd.payu.com/api/v2_1/orders",PayU::createToken());
        $answer=Json::decode($answer);
        if(isset($answer['redirectUri'])){
            $model->payment_link=$answer['redirectUri'];
            $model->save();
            return $this->redirect($answer['redirectUri'], 302);
        }
        else if($answer['status']['codeLiteral']=='ORDER_NOT_UNIQUE'){
            $new_id=$this->cloneDoubledOrder($id);
            return $this->actionPayu($new_id);
        }
        else throw new ForbiddenHttpException(Yii::t('common', 'Something has gone wrong. Make your order again and inform us about the situation.'));
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
