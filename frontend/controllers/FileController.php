<?php

namespace frontend\controllers;

use Yii;
use common\models\File;
use common\models\Subject;
use common\models\UserPermissions;
use common\models\search\FileSearch;
use common\models\search\CommentSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\FileUploadFailedException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','report','download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*
        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        */
        // nobody should access file/index on frontend:
        throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));

    }

    /**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $object=$this->findModel($id);
        $commentsSearchModel = new CommentSearch();
		$comments=$object->getCommentsProvider($id);
		$searchParams=Yii::$app->request->queryParams;
		$searchParams['CommentSearch']['file_id']=$id;
		$comments = $commentsSearchModel->search($searchParams);
		$uid=Yii::$app->user->getId(true);
        $s=Yii::t('common', 'You need to buy access to this file.');
        if($object->createdBy && $uid==$object->createdBy->id) $permission=true;
        else{
            $connection = Yii::$app->getDb();
            if($object->major) $mid=$object->major->id;
			else $mid=$object->subject->major->id;
			$command=$connection->createCommand('SELECT expires FROM '.UserPermissions::tableName().' WHERE major_id='.$mid.' AND user_id='.$uid);
            $result=$command->queryAll();
            $permission=ArrayHelper::map($result,'expires','expires');
            if(empty($permission)) $permission=false;
            else {
                $expires=array_pop($permission);
                if(!$expires) $permission=false;
				else if(time()>$expires){
					$permission=false;
					// remove an obsolete record from the DB:
					$command=$connection->createCommand('DELETE FROM '.UserPermissions::tableName().' WHERE major_id='.$mid.' AND user_id='.$uid);
            		$result=$command->execute();
					$s=Yii::t('common', 'Your access to this file has expired. You need to renew it.');
				}
                else $permission=true;
            }
        }
        if(!$permission) throw new ForbiddenHttpException($s);
        
		return $this->render('view', [
            'model' => $object,
            'comments' => $comments,
			'searchModel' => $commentsSearchModel,
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * Otherwise an exception will be thrown.
     * @return mixed
     * @throws FileUploadFailedException if the upload fails
     */
    public function actionCreate($sid=0,$mid=0)
    {
        $model = new File();
        $path="";
        if ($model->load(Yii::$app->request->post())) {
            $inputfile = UploadedFile::getInstance($model, 'inputfile');
            if(!($model->subject_id)) $model->subject_id=null;
            else $model->major_id=null;
            if(!($model->major_id)) $model->major_id=null;
            else $model->subject_id=null;
            if (!is_null($inputfile)) {
                $model->name = $inputfile->name;
                // generate a unique file name to prevent duplicate filenames:
                //$tmpfilename = Yii::$app->security->generateRandomString().".{$ext}";
                $tmpfilename=$inputfile->name.'_'.time();
                // set upload path as a Yii parameter:
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/../uploaded/';
                $path = Yii::$app->params['uploadPath'] . $tmpfilename;
                $inputfile->saveAs($path); // uses move_uploaded_file();
            } else throw new FileUploadFailedException(Yii::t('common', 'File upload failed.'));
            if ($model->save()) {
                rename($path,Yii::$app->params['uploadPath'] . $model->id . '_'.$inputfile->name);
                if(intval($sid)>0 && $sid===$model->subject_id) return $this->redirect(['/subject/view', 'id' => $model->subject_id, 'uploaded' => 1]);
                else if(intval($mid)>0 && $sid===$model->major_id) return $this->redirect(['/major/view', 'id' => $model->major_id, 'uploaded' => 1]);
                else return $this->redirect(['view', 'id' => $model->id]);
            }  else {
                unlink($path); // remove the file;
                // var_dump($model->getErrors());
                // die();
                throw new FileUploadFailedException(Yii::t('common', 'Could not save the file data to the database.'));
            }
        }
        
        if($sid){
            $sid=intval($sid);
            $mid=0;
        }
        else {
            $sid=0;
            if($mid){
                $mid=intval($mid);
                $sid=0;    
            }
        }

        if($sid) {
            $model->subject_id=$sid;
            $model->major_id=0;
        } else if($mid){
            $model->major_id=$mid;
            $model->subject_id=0;
        }

        $majors = Subject::getMajorList();
        $this->view->params['majors']=$majors;
        $subjects = Subject::getSubjectList();
        $this->view->params['subjects']=$subjects;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->createdBy && $model->createdBy->id!=Yii::$app->user->getId(true)) throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $majors = Subject::getMajorList();
        $this->view->params['majors']=$majors;
        $subjects = Subject::getSubjectList();
        $this->view->params['subjects']=$subjects;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        if($model->createdBy && $model->createdBy->id!=Yii::$app->user->getId(true)) throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
        // remove the file:
        $path = Yii::$app->basePath . '/../uploaded/'.$model->id.'_'.$model->name;
        unlink($path);
        //remove the file info from the DB:
        $model->delete();
        return $this->redirect(['index']);
    }

    public function actionDownload($id){
        $object=$this->findModel($id);
        $uid=Yii::$app->user->getId(true);
        $s='You need to buy access to this file.';
		if($object->createdBy && $uid==$object->createdBy->id) $permission=true;
        else{
            $connection = Yii::$app->getDb();
            if($object->major) $mid=$object->major->id;
			else $mid=$object->subject->major->id;
			$command=$connection->createCommand('SELECT expires FROM '.UserPermissions::tableName().' WHERE major_id='.$mid.' AND user_id='.$uid);
            $result=$command->queryAll();
            $permission=ArrayHelper::map($result,'expires','expires');
            if(empty($permission)) $permission=false;
            else {
                $expires=array_pop($permission);
                if(!$expires) $permission=false;
				else if(time()>$expires){
					$permission=false;
					// remove an obsolete record from the DB:
					$command=$connection->createCommand('DELETE FROM '.UserPermissions::tableName().' WHERE major_id='.$mid.' AND user_id='.$uid);
            		$result=$command->execute();
					$s='Your access to this file has expired. You need to renew it.';
				}
                else $permission=true;
            }
        }
        if(!$permission) throw new ForbiddenHttpException(Yii::t('common', $s));
		$path = Yii::$app->basePath . '/../uploaded/'.$object->id.'_'.$object->name;
        if(file_exists($path)) return Yii::$app->response->sendFile($path);
        else throw new NotFoundHttpException(Yii::t('common', 'The requested file was not found on the server.'));
    }
	
	public function actionReport($id){
		return $this->redirect(['/site/contact', 'fid' => $id]);
	}
	
    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
