<?php

namespace frontend\controllers;

use Yii;
use common\models\Client;
use common\models\User;
use common\models\search\ClientSearch;
use yii\web\Controller;
use yii\web\ServerErrorHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new ClientSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $object=$this->findModel($id);
        $orders=$object->getOrdersProvider();
        $perms=$object->getPermissionsProvider();
        $majors=$object->getPermittedMajorsProvider($perms->getModels());
        $subjects=$object->getPermittedSubjectsProvider($majors->getModels());
        
        return $this->render('view', [
            'model'     => $object,
            'orders'    => $orders,
            'perms'     => $perms,
            'subjects'  => $subjects,
        ]);
    }

    /**
     * Creates a new Client model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Client();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Updates an existing Client model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $client = $this->findModel($id);

        if ($client->load(Yii::$app->request->post())) {
            if($client->save()) return $this->redirect(['view', 'id' => $id]);
            else throw new ServerErrorHttpException(Yii::t('common', 'Could not save the data to the database.'));
        }
		
		if(Yii::$app->user->getId(true)>0 && $client->id==Yii::$app->user->getId(true)){
        	// $langs=array('pl-PL' => 'polski', 'en-US' => 'American English');
            $this->view->params['langs']=Yii::$app->langs;
            return $this->render('update', [
				'model' => $client,
			]);
		} else throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
    }

    /**
     * Deletes an existing Client model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
        // nobody may delete a user from frontend:
        throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findIdentity($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested user does not exist.'));
    }
}
