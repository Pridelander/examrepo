<?php

namespace frontend\controllers;

use Yii;
use common\models\Subject;
use common\models\UserPermissions;
use common\models\search\SubjectSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\Major;
use common\models\University;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * SubjectController implements the CRUD actions for Subject model.
 */
class SubjectController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			'statistics' => [
				'class' => \Klisl\Statistics\AddStatistics::class,
				'actions' => ['index','view'],
			],
		];
    }

    /**
     * Lists all Subject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination=[
            'pageSize' => 20,
        ];
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subject model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $object=$this->findModel($id);
        $files=$object->getFilesProvider($id);
        $uid=Yii::$app->user->getId(true);
        // commented out as we decided to show files:
        /*$s=Yii::t('common', 'You need to buy access to this page.');
        if($object->createdBy && $uid==$object->createdBy->id) $permission=true;
        else{
            $connection = Yii::$app->getDb();
            $command=$connection->createCommand('SELECT expires FROM '.UserPermissions::tableName().' WHERE major_id='.$object->major->id.' AND user_id='.$uid);
            $result=$command->queryAll();
            $permission=ArrayHelper::map($result,'expires','expires');
            if(empty($permission)) $permission=false;
            else {
                $expires=array_pop($permission);
                if(!$expires) $permission=false;
				else if(time()>$expires){
					$permission=false;
					// remove an obsolete record from the DB:
					$sql='DELETE FROM '.UserPermissions::tableName().' WHERE major_id='.$object->major->id.' AND user_id='.$uid;
					$command=$connection->createCommand($sql);
            		$result=$command->execute();
                    $s=Yii::t('common', 'Your access to this page has expired. You need to renew it.');
				}
                else $permission=true;
            }
        }
        if(!$permission) throw new ForbiddenHttpException($s);*/
        return $this->render('view', [
            'model' => $object,
            'files' => $files,
        ]);
    }

    /**
     * Creates a new Subject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subject();
        $majors = Subject::getMajorList();
        $this->view->params['majors']=$majors;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $model->major_id=0;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Subject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $majors = Subject::getMajorList();
        $this->view->params['majors']=$majors;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
		
		if($model->createdBy && $model->createdBy->id==Yii::$app->user->getId(true)){
			return $this->render('update', [
				'model' => $model,
			]);
		} else throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
    }

    /**
     * Deletes an existing Subject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        // return $this->redirect(['index']);
        throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
    }

    /**
     * Finds the Subject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subject::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
