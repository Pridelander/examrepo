<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Major;
use common\models\Order;
use common\models\OrderMajor;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\web\ServerErrorHttpException;

/**
 * Cart controller
 */
class CartController extends Controller
{
   
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','add-to-cart','remove-from-cart','plus','minus','buy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->cart->getPositions(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAddToCart($id) {

        $model = Major::find()->where(['id' => $id, 'active' => 1])->one();
        if ($model) {
            Yii::$app->cart->put($model, 1);
            return $this->redirect(['/major/index']);
        } else throw new NotFoundHttpException();
    
    }

    public function actionRemoveFromCart($id) {
        Yii::$app->cart->removeById($id);
        return $this->redirect(['index']);    
    }
   

    public function actionPlus($id) {
        $position = Yii::$app->cart->getPositionById($id);
        Yii::$app->cart->update($position, $position->getQuantity() + 1);
        return $this->redirect(['index']);    
    }

    public function actionMinus($id) {
        $position = Yii::$app->cart->getPositionById($id);
        Yii::$app->cart->update($position, $position->getQuantity() - 1);
        return $this->redirect(['index']);    
    }

    public function actionBuy(){
        $ordered=Yii::$app->cart->getOrderedWithQuantities();
        $order=new Order();
        $order->client_id=Yii::$app->user->getId(true);
        if($order->save()){
            foreach($ordered as $mid => $mq){
                $om=new OrderMajor();
                $om->order_id=$order->id;
                $om->major_id=$mid;
                $om->quantity=$mq;
                $x=$om->save();
                if(!$x){
                    $order->delete(); // this will also remove om entries thanks to our wonderful DBMS;
                    throw new ServerErrorHttpException(Yii::t('common', 'Could not save order data to the database.'));
                    break;
                }
            }
            Yii::$app->cart->removeAll();
            return $this->redirect(['order/payu', 'id' => $order->id]);
        }
        else throw new ServerErrorHttpException(Yii::t('common', 'Could not save the data to the database.'));
        /*else {
            var_dump($order->client_id);
            var_dump($order->getErrors());
            die();
        }*/
    }
}
