<?php

namespace frontend\controllers;

use Yii;
use common\models\Major;
use common\models\UserPermissions;
use common\models\search\MajorSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * MajorController implements the CRUD actions for Major model.
 */
class MajorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			'statistics' => [
				'class' => \Klisl\Statistics\AddStatistics::class,
				'actions' => ['index','view'],
			],
        ];
    }

    /**
     * Lists all Major models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MajorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination=[
            'pageSize' => 20,
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Major model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $object = $this->findModel($id);
        $subjects=$object->getSubjectsProvider($id);
        $files=$object->getFilesProvider($id);
        $uid=Yii::$app->user->getId(true);
		// commented out as we decided to show files:
        /*if($object->createdBy && $uid==$object->createdBy->id) $permission=true;
        else{
            $connection = Yii::$app->getDb();
            $command=$connection->createCommand('SELECT expires FROM '.UserPermissions::tableName().' WHERE major_id='.$id.' AND user_id='.$uid);
            $result=$command->queryAll();
            $permission=ArrayHelper::map($result,'expires','expires');
            if(empty($permission)) $permission=false;
            else {
                $expires=array_pop($permission);
                if(!$expires) $permission=false;
				else if(time()>$expires){
					$permission=false;
					// remove an obsolete record from the DB:
					$command=$connection->createCommand('DELETE FROM '.UserPermissions::tableName().' WHERE major_id='.$id.' AND user_id='.$uid);
            		$result=$command->execute();
				}
                else $permission=true;
            }
        }*/
        return $this->render('view', [
            // przesłanie zmiennych do templatki jak w phpBB:
            'model' => $object,
            'subjects' => $subjects,
            'files' => $files,
            //'permission' => $permission,
        ]);
    }

    /**
     * Creates a new Major model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Major();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Major model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
		
		if($model->createdBy && $model->createdBy->id==Yii::$app->user->getId(true)){
			return $this->render('update', [
				'model' => $model,
			]);
		} else throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
    }

    /**
     * Deletes an existing Major model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        // return $this->redirect(['index']);
        throw new ForbiddenHttpException(Yii::t('common', 'You have no access to this page.'));
    }

    /**
     * Finds the Major model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Major the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Major::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }
}
