<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use common\models\Comment;

/* @var $this yii\web\View */
/* @var $model common\models\File */

$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(
    "$('#show-comment').click(function() { 
		$('#add-comment').css('display','initial');
		$('#show-comment').css('display','none');
	});"
);

?>
<div class="file-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Report'), ['report', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <!-- <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> -->
        <!-- <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'subject_id',
            'major_id',
            ['attribute' => 'created_by',
            'value' => ($model->createdBy)?$model->createdBy->username:''],
            'created_at',
            //'updated_by',
            //'updated_at',
        ],
    ]) ?>
    
    <?= Html::a(Yii::t('common', 'Download'), ['download', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    <br/>
    
    <h3><?php echo Yii::t('common', 'Comments'); ?></h3>

    <?= GridView::widget([
        'dataProvider' => $comments,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            
            ['attribute' => 'user.username',
            'value' => 'user.username'],
            'content:ntext',
			'datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    }
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/comment/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/comment/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/comment/delete','id'=>$model->id]);
                    }
      
                }
            ],
        ],
    ]) ?>
	
	<a id="show-comment" class="btn btn-primary" href="#show-comment"><?php echo Yii::t('common','Add comment'); ?></a>
	<div id="add-comment" style="display:none;">
	<br/>
    
    <h3><?php echo Yii::t('common', 'Add comment'); ?></h3>
	
	<?php
	$comment = new Comment();
	?>
	<?= $this->render('/comment/_form',[
		'model'=>$comment,
		'fid'=>$model->id,
		'action' => '/comment/create',
	]) ?>
	</div>
</div>
