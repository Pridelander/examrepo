<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create File'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/file/download','id'=>$model->id]));
                },
            ],
            ['attribute' => 'subject.name',
            'value' => 'subject.name'],
            ['attribute' => 'major.name',
            'value' => 'major.name'],
            ['attribute' => 'createdBy.username',
            'value' => 'createdBy.username'],
            //'created_at',
            //'updated_by',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}   {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    },
                    'upload' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, [
                                    'title' => Yii::t('common', 'Upload'),
                        ]);
                    },
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/file/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/file/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/file/delete','id'=>$model->id]);
                    }
                    if ($action === 'upload') {
                        return Url::to(['/file/create','sid'=>$model->id]);
                    }
                }
            ],
        ],
    ]); ?>
</div>
