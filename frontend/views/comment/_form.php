<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php
		if(isset($action)){
			$form = ActiveForm::begin(['action' => $action]);
		}
		else $form = ActiveForm::begin();
	?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>Yii::$app->user->getId(true)])->label(false) ?>

    <?= $form->field($model, 'file_id')->hiddenInput(['value'=>$fid])->label(false) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
