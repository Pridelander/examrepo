<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */

$this->title = Yii::t('common', 'Update Comment: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'File No. ').$model->id, 'url' => ['file/view', 'id' => $model->file_id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update Comment');
?>
<div class="comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'fid' => $model->file_id,
    ]) ?>

</div>
