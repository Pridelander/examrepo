<?php 

use yii\helpers\Url;
use yii\helpers\Html;


?>

<div class="col-sm-6">
	<h3><a href="<?= Url::to(['view', 'id'=> $model->id]) ?>"><?= $model->name ?></a></h3>

	<div><?= Yii::$app->formatter->asDecimal($model->price, 2) ?> zł</div>

	<?= Html::a("Dodaj", Url::to(['/cart/add-to-cart', 'id' => $model->id]), ['class' => 'btn btn-success']) ?>
</div>