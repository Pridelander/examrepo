<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MajorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Majors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="major-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create Major'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Major Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/major/view','id'=>$model->id]));
                },
            ],
            // replace the id with the name:
            [
                'attribute' => 'university.name',
                'label' => Yii::t('common', 'University Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->university->name, Url::to(['/university/view','id'=>$model->university->id]));
                },
            ],
            'price',
            'tax',
            //'description:ntext',
            //'active',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}   {update}    {buy}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    },
                    'upload' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, [
                                    'title' => Yii::t('common', 'Upload'),
                        ]);
                    },
                    'buy' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-shopping-cart"></span>', $url, [
                                    'title' => Yii::t('common', 'Buy'),
                        ]);
                    },
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/major/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/major/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/major/delete','id'=>$model->id]);
                    }
                    if ($action === 'buy') {
                        return Url::to(['/cart/add-to-cart','id'=>$model->id]);
                    }
                    if ($action === 'upload') {
                        return Url::to(['/file/create','sid'=>$model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
