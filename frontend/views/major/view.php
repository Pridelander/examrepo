<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Major */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Majors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="major-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <!-- <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?> -->
        <?= Html::a(Yii::t('common', 'Upload'), ['/file/create', 'mid' => $model->id], ['class' => 'btn btn-warning']) ?>
		<?= Html::a(Yii::t('common', 'Buy'), ['/cart/add-to-cart', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            ['attribute' => 'university_id',
            'value' => $model->university->name],
            'name:ntext',
            'price',
            'tax',
            'description:ntext',
        ],
    ]) ?>

    <br/>
    
    <h3><?php echo Yii::t('common', 'Subjects'); ?></h3>

    <?= GridView::widget([
        'dataProvider' => $subjects,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/subject/view','id'=>$model->id]));
                },
            ],
            ['attribute' => 'major.name',
            'value' => 'major.name'],
            'description:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    }
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/subject/view','id'=>$model->id]);
                    }
      
                    if ($action === 'update') {
                        return Url::to(['/subject/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/subject/delete','id'=>$model->id]);
                    }
      
                }
            ],
        ],
    ]) ?>

    <br/>
    <h3><?php echo Yii::t('common', 'Files'); ?></h3>

    <?= GridView::widget([
        'dataProvider' => $files,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/file/view','id'=>$model->id]));
                    // return Url::to(['/subject/view','id'=>$model->id]);
                },
            ],
            'created_at',
            'created_by',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}   {download}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    },
                    'download' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-download"></span>', $url, [
                                    'title' => Yii::t('common', 'Download'),
                        ]);
                    },
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/file/view','id'=>$model->id]);
                    }
                    if ($action === 'update') {
                        return Url::to(['/file/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/file/delete','id'=>$model->id]);
                    }
                    if ($action === 'download') {
                        return Url::to(['/file/download','id'=>$model->id]);
                    }
                }
            ],
        ],
    ]) ?>
</div>
