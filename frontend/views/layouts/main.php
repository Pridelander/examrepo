<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use common\widgets\Alert;
use frontend\models\LangSelectForm;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('common',Yii::$app->name),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => Yii::t('common','Home'), 'url' => ['/site/index']],
        ['label' => Yii::t('common','Contact'), 'url' => ['/site/contact']],
    ];

    $menuItems[] = [
        'label' => Yii::t('common','Contents'),
        'items' => [
            ['label' => Yii::t('common','Universities'), 'url' => ['/university/index']],
            ['label' => Yii::t('common','Majors'), 'url' => ['/major/index']],
            ['label' => Yii::t('common','Subjects'), 'url' => ['/subject/index']],
        ],
    ];

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('common','Signup'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => Yii::t('common','Login'), 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => Yii::t('common','My Orders'), 'url' => ['/order/index']];
		$menuItems[] = '<li><a href="/cart/index"><span class="glyphicon glyphicon-shopping-cart">(' . Yii::$app->cart->getCount() . ')</span></a></li>';
        $logoutForm = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('common','Logout').' (' . Yii::$app->user->identity->username . ')',
                ['id' => 'button-logout']
            )
            . Html::endForm()
            . '</li>';
        $menuItems[] = [
            'label' => Yii::$app->user->identity->username,
            'items' => [
                ['label' => Yii::t('common','Profile'), 'url' => ['/profile/view', 'id' => Yii::$app->user->identity->id]],
                ['label' => Yii::t('common','Edit profile'), 'url' => ['/profile/update', 'id' => Yii::$app->user->identity->id]],
                ['label' => Yii::t('common','E-mail change'), 'url' => ['/site/email-change']],
                ['label' => Yii::t('common','Password change'), 'url' => ['/site/password-change']],
                // ['label' => 'Logout ('.Yii::$app->user->identity->username.')', 'url' => ['/site/logout']],
                $logoutForm,
            ],
        ];
    }
    
    $menuItems[] = '<li><a href="/site/language-select"><span class="glyphicon glyphicon-comment"></span></a></li>';
        
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
		<?php
		$model=new LangSelectForm();
		?>
		<?= $this->render('/site/langSelectMin', [
			'model' => $model,
			'langs' => Yii::$app->langs,
		]) ?>
	
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
