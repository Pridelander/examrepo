<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

?>

<h1> <?php echo Yii::t('common','List of products in your cart:'); ?></h1>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'university_id',
            'name:ntext',
            'price',
			'tax',
			[
				'attribute' => 'id',
				'label' => Yii::t('common','Months of subscription'),
				'value' => function ($model) {
					return $model->getQuantity();
				}
			],
            //'description:ntext',
            //'availability',
            //'thumbnail:ntext',
            //'active',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{plus}	{minus}		{remove}',
				
				'buttons' => [
					'plus' => function ($url, $model, $key) {
						return Html::a('<span class="glyphicon glyphicon-plus"></span>', Url::to(['/cart/plus', 'id' => $model->id]));
					},
					'minus' => function ($url, $model, $key) {
						return Html::a('<span class="glyphicon glyphicon-minus"></span>', Url::to(['/cart/minus', 'id' => $model->id]));
					},
					'remove' => function ($url, $model, $key) {
						return Html::a('<span class="glyphicon glyphicon-remove"></span>', Url::to(['/cart/remove-from-cart', 'id' => $model->id]));
					}
				]
			],
        ],
	]); 
?>
<p>
	<?php echo Yii::t('common','Total cost:'); ?> <?= Yii::$app->cart->getCost() ?>
</p>

<?php if (!Yii::$app->cart->getIsEmpty()) {
?>
	<p>
		<?= Html::a(Yii::t('common', 'Buy all'), ['buy'], ['class' => 'btn btn-primary']) ?>
	</p>
<?php } ?>