<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = Yii::t('common', 'Update Client: ') . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');

$new=(Yii::$app->request->get('new'))?true:false;
?>
<div class="client-update">
    
    <?php if($new){ ?>
    <p class="btn btn-success center-block" style="font-size:1.3em;"> <?php echo Yii::t('common', 'You have registered successfully. Please provide us with some information about you.'); ?> </p>
    <?php } else {?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php } ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
