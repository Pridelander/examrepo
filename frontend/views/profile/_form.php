<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'username')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'fname')->textInput() ?>

    <?= $form->field($model, 'lname')->textInput() ?>

    <?= $form->field($model, 'language')->dropDownList($this->params['langs'],['options' => [$model->language => ['selected'=>true]]]) ?>				

    <!-- <?= $form->field($model, 'discount')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div style="margin-top:20px;">
        <p style="text-align:center; display: inline-block;"><a class="btn btn-primary" href="/site/email-change"><?php echo Yii::t('common','E-mail change'); ?></a></p>
        <p style="text-align:center; display: inline-block; margin-left: 15px;"><a class="btn btn-primary" href="/site/password-change"><?php echo Yii::t('common','Password change'); ?></a></p>
    </div>

</div>
