<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = Yii::t('common', 'Order no. ').$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if($model->status==1) { ?>
        <p>
            <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a(Yii::t('common', 'Pay'), ['/order/payu', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        </p>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'client_id',
            ['attribute' => 'client_id',
            'value' => $model->client->fname],
            ['attribute' => 'client_id',
            'value' => $model->client->lname],
            // 'name:ntext',
            // 'status',
            ['attribute' => 'status',
            'value' => function($model){
                switch($model->status){
                    case 1: return Yii::t('common', 'Created');
                    case 2: return Yii::t('common', 'Paid');
                    case 3: return Yii::t('common', 'Rejected by PayU');
					case 4: return Yii::t('common', 'Error on PayU side');
					default: return Yii::t('common', 'An error occured');
                }
            }],
            // 'payment_type_id',
            // 'created_by',
            // 'created_at',
            ['attribute' => 'updated_by',
            'value' => ($model->updatedBy)?$model->updatedBy->username:''],
            // 'updated_by',
            'updated_at',
        ],
    ]) ?>

    <br/>
    
    <h3><?php echo Yii::t('common', 'Ordered majors'); ?></h3>

    <?= GridView::widget([
        'dataProvider' => $oms,
        //'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->major->name, Url::to(['/major/view','id'=>$model->major->id]));
                },
            ],
            // ['attribute' => 'major.name',
            // 'value' => 'major.name'],
            [
                'label' => Yii::t('common', 'Months of subscription'),
                // 'attribute' => 'quantity', // gonna prevent sorting by this attribute;
                'value' => 'quantity',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    }
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/major/view','id'=>$model->major->id]);
                    }
      
                    if ($action === 'update') {
                        return Url::to(['/major/update','id'=>$model->major->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/major/delete','id'=>$model->major->id]);
                    }
      
                }
            ],
        ],
    ]) ?>

    <br/>
    
    <h3><?php echo Yii::t('common', 'Ordered subjects'); ?></h3>

    <?= GridView::widget([
        'dataProvider' => $subjects,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'name',
            [
                'label' => Yii::t('common', 'Name'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->name, Url::to(['/subject/view','id'=>$model->id]));
                },
            ],
            ['attribute' => 'major.name',
            'value' => 'major.name'],
            'description:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('common', 'Actions'),
                //'headerOptions' => ['style' => 'color:#337ab7'],
                // 'template' => '{view}{update}{delete}',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('common', 'View'),
                        ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('common', 'Update'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('common', 'Delete'),
                        ]);
                    }
      
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to(['/subject/view','id'=>$model->id]);
                    }
      
                    if ($action === 'update') {
                        return Url::to(['/subject/update','id'=>$model->id]);
                    }
                    if ($action === 'delete') {
                        return Url::to(['/subject/delete','id'=>$model->id]);
                    }
      
                }
            ],
        ],
    ]) ?>

</div>
