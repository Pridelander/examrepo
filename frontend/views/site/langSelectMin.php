<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="row">
	<div class="col-lg-12" style="height:39px;">
		<?php $form = ActiveForm::begin(['id' => 'form-lang-select', 'action' => '/site/language-select']); ?>

			<div style="margin: 0 auto; text-align:center; ">
				<?= $form->field($model, 'lang', ['options'=>['style'=>'display:inline-block;']])->dropDownList($langs,['options' => [Yii::$app->language => ['selected'=>true]],'style'=>'width:initial;height:initial;display:inline-block;font-size:12px;'])->label(Yii::t('common','Language'),['style'=>'font-size:12px;']) ?>
			<div style="display:inline-block;">
				<?= Html::submitButton(Yii::t('common','Submit'), ['class' => 'btn btn-primary', 'name' => 'signup-button', 'style'=>'font-size:11px;']) ?>
			</div>
			</div>

		<?php ActiveForm::end(); ?>
	</div>
</div>