<?php

/* @var $this yii\web\View */

$this->title = Yii::t('common',Yii::$app->name);
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo Yii::t('common',Yii::$app->name); ?></h1>

        <p class="lead"><?php echo Yii::t('common','Welcome to our community. Join us to exchange exam materials with others.'); ?></p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <p style="text-align:center;"><a class="btn btn-lg btn-primary" href="/university/index"><?php echo Yii::t('common','Universities'); ?></a></p>
            </div>
            <div class="col-lg-4">
                <p style="text-align:center;"><a class="btn btn-lg btn-primary" href="/major/index"><?php echo Yii::t('common','Majors'); ?></a></p>
            </div>
            <div class="col-lg-4">
                <p style="text-align:center;"><a class="btn btn-lg btn-primary" href="/subject/index"><?php echo Yii::t('common','Subjects'); ?></a></p>
            </div>
        </div>

    </div>
</div>