<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('common','Select application language');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-email-change">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-lang-select']); ?>
    
                <?= $form->field($model, 'lang')->dropDownList($this->params['langs'],['options' => [Yii::$app->language => ['selected'=>true]]]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('common','Submit'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
