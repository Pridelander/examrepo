<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('common','Password change');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-password-change">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    
                <?= $form->field($model, 'oldpassword')->passwordInput(['autofocus' => true]) ?>
                <br/><hr/><br/>
                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'repeatpassword')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('common','Submit'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
