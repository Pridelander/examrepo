<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('common', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?php echo Yii::t('common','Please fill out the following fields to signup:'); ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'repeatpassword')->passwordInput() ?>

				<hr/>
				
				<?= $form->field($model, 'fname')->textInput() ?>

				<?= $form->field($model, 'lname')->textInput() ?>

				<?= $form->field($model, 'language')->dropDownList($this->params['langs'],['options' => [Yii::$app->language => ['selected'=>true]]]) ?>				
                
				<div class="form-group">
                    <?= Html::submitButton(Yii::t('common','Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
