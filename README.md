## About the project

The project was created back in 2019, originally as my final pratical exam in the subject of E-commerce at MSc studies at Poznań University of Technology. It is created with PHP 7.1 and Yii 2 framework.  
The idea behind the project is to facilitate sharing exam materials (e.g. teaching aids) between students and enable them to earn some money on them.  
The project has never gone public and may not even be useful anymore in the epoch of AI.

The repository is public just to allow anybody to see a sample of my code. I have put here only the most important files (models, custom controllers, views and some basic Yii files) but they are not enough to build or run the project.